const jwq = require('jsonwebtoken')
const { SECRET } = require('./../bd/config')

const verificaToken = (req, res, next) => {
  let token = req.headers['token']
  
  jwq.verify(token, SECRET, (err, decoded) => {
    if(err) {
      return res.status(401).json({
        err: {
          message: 'Token no válido'
        }
      })
    }
    req.usuario = decoded.data;
    // req.usuario = { usuario: 'admin', password: '0105'}
    next()
  } )
}

module.exports = { verificaToken }