const { sql, poolPromise } = require('./../bd/bd')

class OrdenesModelo {
  async seleccionarOrdenes (desde, hasta, codigoTipoUsuario, codigoUsuario) {
    try {
      let pool = await poolPromise
      let result = await pool.request()
        .input('desde', sql.Int, desde)
        .input('hasta', sql.Int, hasta)
        .input('codigoTipoUsuario', sql.Int, codigoTipoUsuario)
        .input('codigoUsuario', sql.Int, codigoUsuario)
        .execute('SP_JobStatusOrdenes')
        return result.recordset
      } catch ( err ) { throw err.message }
    }
    
    async seleccionarOrden (id, subid) {
      try {
        let pool = await poolPromise
        let result = await pool.request()
        .input('id', sql.NVarChar, id)
        .input('subid', sql.NVarChar, subid)
        .execute('SP_JobStatusOrden')
        return result.recordset
      } catch ( err ) { throw err.message }
    }
    
    async seleccionarCampos () {
      try {
        let pool = await poolPromise
        let result = await pool.request()
        .query(`SELECT * FROM JobStatusCampos ORDER BY orden`)
        return result.recordset
      } catch ( err ) { throw err.message }
    }
    
    async buscarOrdenes (desde, hasta, codigoTipoUsuario, codigoUsuario, campo, filtro) {
      try {
        let pool = await poolPromise
        let result = await pool.request()
        .input('desde', sql.Int, desde)
        .input('hasta', sql.Int, hasta)
        .input('codigoTipoUsuario', sql.Int, codigoTipoUsuario)
        .input('codigoUsuario', sql.Int, codigoUsuario)
        .input('campo', sql.NVarChar, campo)
        .input('filtro', sql.NVarChar, filtro)
        .execute('SP_JobStatusBuscarOrdenes')
      return result.recordset
    } catch ( err ) { throw err.message }
  }
}

module.exports = OrdenesModelo