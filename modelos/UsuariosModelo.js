const UsuariosServicio = require('../servicios/UsuariosServicio')
const { sql, poolPromise } = require('./../bd/bd')

class UsuariosModelo {

  async seleccionarUsuarioNombre ( body ) {
    try {
      let pool = await poolPromise
      let result = await pool.request()
        .input('usuario', sql.NVarChar, body.usuario)
        .query(`SELECT TOP 1 * FROM Usuarios WHERE nombre = @usuario`)
      return result.recordset[0]
    } catch ( err ) { throw err.message }
  }
}

module.exports = UsuariosModelo