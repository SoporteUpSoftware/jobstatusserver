const express = require('express')
const { verificaToken } = require('./../middlewares/auth')
const Respuesta = require('./utils/Respuesta')
const OrdenesServicio = require('./../servicios/OrdenesServicio')

const app = express()
const respuesta = new Respuesta()
const ordenesServicio = new OrdenesServicio()

app.get('/ordenes/:pagina/:codigoTipoUsuario/:codigoUsuario', verificaToken, async (req, res) => {
  try {
    let ordenes = await ordenesServicio.seleccionarOrdenes(req.params.pagina, req.params.codigoTipoUsuario, req.params.codigoUsuario)
    return respuesta.exitosa(res, ordenes)
  } catch ( err ) { return respuesta.errorServidor(res, err) }
})

app.get('/buscarOrdenes/:pagina/:codigoTipoUsuario/:codigoUsuario/:campo?/:filtro?', verificaToken, async (req, res) => {
  try {
    let ordenes = await ordenesServicio.buscarOrdenes(req.params.pagina, req.params.codigoTipoUsuario, req.params.codigoUsuario, req.params.campo, req.params.filtro)
    return respuesta.exitosa(res, ordenes)
  } catch ( err ) { return respuesta.errorServidor(res, err) }
})

app.get('/orden/:id/:subid?', verificaToken, async (req, res) => {
  try {
    let ordenes = await ordenesServicio.seleccionarOrden(req.params.id, req.params.subid)
    return respuesta.exitosa(res, ordenes)
  } catch ( err ) { return respuesta.errorServidor(res, err) }
})

module.exports = app