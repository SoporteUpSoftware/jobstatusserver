const express = require('express')
const { verificaToken } = require('./../middlewares/auth')
const Respuesta = require('./utils/Respuesta')
const UsuariosServicio = require('./../servicios/UsuariosServicio')

const app = express()
const respuesta = new Respuesta()
const usuariosServicio = new UsuariosServicio()

app.post('/login', async (req, res) => {
  try {
    let usuario = await usuariosServicio.login(req.body)
    return respuesta.exitosa(res, usuario)
  } catch ( err ) { return respuesta.errorServidor(res, err)}
})

app.get('/token', verificaToken, async (req, res) => {
  try {
    let token = await usuariosServicio.refrescarToken(req.usuario)
    return respuesta.exitosa(res, token)
  } catch ( err ) { return respuesta.errorServidor(res, err)}
})


module.exports = app