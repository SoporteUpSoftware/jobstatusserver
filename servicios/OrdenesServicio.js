const OrdenesModelo = require('./../modelos/OrdenesModelo')
const formatDDMMYYYY = require('./utilsFechas')

const ordenesModelo = new OrdenesModelo()

class OrdenesServicio {

  async seleccionarOrdenes (pagina, codigoTipoUsuario, codigoUsuario) {
    try {
      let desde = 1, cantidadRegistros = 50, hasta
      if(pagina == 0) {
        hasta = 10000
      } else if( pagina == 1) {
        hasta = cantidadRegistros        
      }else {
        desde = (pagina - 1) * cantidadRegistros +1
        hasta = pagina * cantidadRegistros 
      }

      

      let ordenes = await ordenesModelo.seleccionarOrdenes(desde, hasta, codigoTipoUsuario, codigoUsuario)
      // let ordenesFechaFormateada = formatDDMMYYYY(ordenes, 'fechaPrometida')
      let campos = await ordenesModelo.seleccionarCampos()
      return { ordenes: ordenes || ordenes, campos }
    } catch ( err ) { throw err }
  }
  
  async buscarOrdenes (pagina, codigoTipoUsuario, codigoUsuario, campo, filtro) {
    try {
      let desde = 1, cantidadRegistros = 50, hasta
      if(pagina == 0) {
        hasta = 10000
      } else if( pagina == 1) {
        hasta = cantidadRegistros        
      }else {
        desde = (pagina - 1) * cantidadRegistros +1
        hasta = pagina * cantidadRegistros 
      }

      if(campo == 'fechaPrometida' || campo == 'fechaEstimadaEntrega') {
        filtro = filtro.split('-').join('/')
      }

      let  ordenes = await ordenesModelo.buscarOrdenes(desde, hasta, codigoTipoUsuario, codigoUsuario, campo, filtro)
      // let ordenesFechaFormateada = formatDDMMYYYY(ordenes, 'fechaPrometida')
      return  ordenes
    } catch ( err ) { throw err }
  }
  
  async seleccionarOrden (id, subid) {
    try {
      let orden = await ordenesModelo.seleccionarOrden(id, subid)
      return orden
    } catch ( err ) { throw err }
  }
}

module.exports = OrdenesServicio