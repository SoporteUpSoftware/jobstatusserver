const UsuariosModelo = require('./../modelos/UsuariosModelo')
const { SECRET } = require('./../bd/config')
const jwt = require('jsonwebtoken')

const usuariosModelo = new UsuariosModelo()

class UsuariosServicio {

  firmarJWT ( usuario ) {
    return jwt.sign({ data: usuario, exp: Math.floor(Date.now() / 1000) + (60 * 60 * 1000)}, SECRET)
  }

  async login ( body ) {
    try {
      let usuarioBD = await usuariosModelo.seleccionarUsuarioNombre(body)

      // si no existe el usuario
      if(!usuarioBD) throw { message: 'Usuario y/o contraseña incorrectos'}

      // si no coincide la contraseña
      if(usuarioBD.Password != body.password) throw { message: 'Usuario y/o contraseña incorrectos'}

      delete usuarioBD.Password

      // crear token
      let token = this.firmarJWT(usuarioBD)


      return {
        usuario: usuarioBD,
        token
      }
    } catch ( err ) { throw err }
  }

  async refrescarToken (usuario) {
    try {
      // crear token
      let token = this.firmarJWT(usuario)
      
      return {
        usuario: usuario,
        token
      }   
    } catch ( err ) { throw err }
  }

}

module.exports = UsuariosServicio