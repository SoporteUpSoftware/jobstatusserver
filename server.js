const express = require('express')
const cors = require('cors')
const path = require('path')
const history = require('connect-history-api-fallback');
const bodyParser = require('body-parser')
require('dotenv').config()

const app = express()

app.use(bodyParser.urlencoded( { extended: true} ))
app.use(bodyParser.json())
app.use(cors())

//These 2 lines make sure that vue and express app are coming from the same server. 
app.use('/public', express.static(path.join(__dirname,"/public/"))); 
app.use(history({verbose: true, index: '/'}))
app.use('/public', express.static(path.join(__dirname,"/public/"))); 


app.get('/', function(req,res) {
  res.sendFile('index.html', { root: path.join(__dirname, '/public/') });
});



app.get('/', (req, res) => res.json({ok: true}))
app.use(require('./rutas/ordenes'))
app.use(require('./rutas/usuarios'))


app.listen(process.env.PORT, (err) => {
  if(err) return console.log(err)
  console.log(`Servidor JobStatus escuchando en el puerto ${process.env.PORT}`)
})