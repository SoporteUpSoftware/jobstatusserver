require('dotenv').config()

const config = {
  user: process.env.USUARIO_BD,
  password: process.env.PASSWORD_BD,
  server: process.env.SERVIDOR_BD,
  database: process.env.NOMBRE_BD,
  pool: {
    max: 10,
    min: 0,
    idleTimeoutMills: 30000,
  },
  options: {
    useUTC: true,
    encrypt: false,
    enableArithAbort: false
  }
}

const SECRET = 'un_elefante_se_balanceaba_sobre_la_tela_de_una_araña'

module.exports = {config, SECRET }