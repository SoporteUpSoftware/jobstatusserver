const sql = require('mssql')
const { config } = require('./config')

const poolPromise = new sql.ConnectionPool(config)
  .connect()
  .then( pool => {
    console.log('Base de datos conectada')
    return pool
  })
  .catch( err => console.log(`Error en la conexion a la base de datos, revise los datos de conxion: ${err}`))

module.exports = { sql, poolPromise }